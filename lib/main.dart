import 'dart:async';

import 'package:flutter/material.dart';

import 'helpers/network.dart';
import 'helpers/database.dart';
import 'helpers/exceptions.dart';
import 'models/user.dart';

import 'pages/login.dart';
import 'pages/splash.dart';
import 'pages/judge.dart';

Network network = new Network();
User user       = new User(null, null);
DB database     = new DB();

void main() => runApp(new Main());

class Main extends StatelessWidget {

    /// Gets information about logged in users
    /// and specifies whether to show login page
    /// or start page
    Future<User> getStartingPage() async {
        try {
            await user.testCredentials().then((_user) {
                return _user;
            }).catchError((error) {
                throw error;
            });
        }
        on UserInvalid catch (exception) {
            throw exception;
        }
        catch (exception) {
            throw exception;
        }
        return user;
    }

    Widget build(BuildContext context) {

        return new FutureBuilder(
            future: getStartingPage(),
            builder: (BuildContext context, AsyncSnapshot<User> snapshot) {
                if (snapshot.hasData) {
                    return JudgeApp();
                }
                else if (snapshot.hasError) {
                    return LoginPage();
                }
                return new SplashPage();
            }
        );

    }

}
