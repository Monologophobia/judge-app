import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';

import '../helpers/exceptions.dart';
import '../main.dart';

class User {

    int id;
    String email;
    String password;
    List<Map> events;
    List<Map> teams;
    List<Map> heats;

    SharedPreferences prefs;

    User(String _email, String _password) {
        this.email = _email;
        this.password = _password;
    }

    Future<User> testCredentials() async {

        // get user from sharedprefs
        this.prefs       = await SharedPreferences.getInstance();
        String _email    = this.prefs.get('email');
        String _password = this.prefs.get('password');

        // if no user has logged in before
        if (_email == null || _password == null) throw new UserInvalid("No User Credentials stored");

        await this.login(_email, _password);

        return this;

    }

    /// log the user in
    Future<void> login(String email, String password) async {

        this.email    = email;
        this.password = password;

        // test login
        await network.post('login', this);

        // Store user details
        if (this.prefs == null) this.prefs = await SharedPreferences.getInstance();
        this.prefs.setString('email', this.email);
        this.prefs.setString('password', this.password);

        // Retrieve events
        await this._getEvents();
        // Retrieve teams
        await this._getTeams();
        // Retrieve heats
        await this._getHeats();

    }

    Future<void> _getEvents() async {

        Map _events = await network.get('events');

        if (_events['response'].length > 0) {

            List<Map> _list = [];

            for (Map _event in _events['response']) {
                // parse integers
                if (!(_event['id'] is int)) _event['id'] = int.parse(_event['id']);
                if (!(_event['type'] is int)) _event['type'] = int.parse(_event['type']);
                _list.add(_event);
            }

            this.events = _list;

        }

    }

    Future<void> _getTeams() async {

        Map _events = await network.get('teams');

        if (_events['response'].length > 0) {

            List<Map> _list = [];

            for (Map _team in _events['response']) {
                // parse integers
                if (!(_team['id'] is int)) _team['id'] = int.parse(_team['id']);
                if (!(_team['heat_id'] is int)) _team['heat_id'] = int.parse(_team['heat_id']);
                _list.add(_team);
            }

            this.teams = _list;

        }

    }

    Future<void> _getHeats() async {

        Map _heats = await network.get('heats');

        if (_heats['response'].length > 0) {

            List<Map> _list = [];

            for (Map _heat in _heats['response']) {
                // parse integers
                if (!(_heat['id'] is int)) _heat['id'] = int.parse(_heat['id']);
                _list.add(_heat);
            }

            this.heats = _list;

        }

    }

    Future<void> logout() async {
        if (this.prefs == null) this.prefs = await SharedPreferences.getInstance();
        this.prefs.setString('email', null);
        this.prefs.setString('password', null);
    }

}