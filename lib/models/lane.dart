import 'team.dart';

class Lane {

    int id;
    var name;
    Team team;

    Lane(int id, int teamId) {
        this.id = id;
        this.name = "Lane " + id.toString();
        this.team = team;
    }

}

class TeamNotFound implements Exception {
    String cause;
    TeamNotFound(this.cause);
}

class LaneList {
    
}