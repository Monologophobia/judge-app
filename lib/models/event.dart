enum Type {
    increments, timed, 
}

class Event {

    int id;
    var name;
    var description;
    var mainText;
    var incrementText;
    var increments;
    var ladder;
    int type;
    Map stages;
    bool halves = false;

    List<Map> types = [{
        1: 'increments',
        2: 'timed',
        3: 'multiple tallies',
        4: 'set increments',
        5: 'ladder (up and down increments)',
        6: 'laps'
    }];

    // constructor
    Event(int _id, var _name, var _description, int _type, Map _data) {

        this.id          = _id;
        this.name        = _name;
        this.description = _description;
        this.type        = _type;

        // increments or set increments
        if (this.type == 1 || this.type == 4) {

            // set increments
            if (this.type == 4) {
                List _increments = [];
                for (var _increment in _data['increments']) {
                    if (_increment is String) _increment = int.parse(_increment);
                    _increments.add(_increment);
                }
                _data['increments'] = _increments;
            }

            this.increments    = _data['increments'];
            this.mainText      = _data['mainText'];
            this.incrementText = _data['incrementText'];

        }

        // timed
        if (this.type == 2) { }

        // multiple tallies
        if (this.type == 3) {
            this.stages = {};
            _data.forEach((key, value) {
                int _key   = (key is int ? key : int.parse(key));
                this.stages[_key] = value;
            });
        }

        // set increments. dealt with above
        if (this.type == 4) { }

        // ladder (also handles final increments if they exist)
        if (this.type == 5) {
            this.increments = [];
            this.ladder = [];
            if (_data.containsKey('increments') && _data['increments'].length > 0) {
                for (var _increment in _data['increments']) {
                    Map _incrementData = {'name': _increment['name'], 'index': _increment['index']};
                    this.increments.add(_incrementData);
                }
            }
            for (var _increment in _data['ladder']) {
                this.ladder.add(_increment);
            }
            this.mainText      = _data['mainText'];
            this.incrementText = _data['incrementText'];
        }

        // does this event involve half measures?
        // only useable for tallies type
        if (this.type == 3) {
            if (_data.containsKey('halves') && _data['halves'] == true) {
                this.halves = true;
            }
        }

        if (this.type == 6) {
            this.mainText   = _data['mainText'].toString();
            this.increments = num.parse(_data['increments']);
        }

    }

    Map generateResults(Map _data) {

        Map _return = _data;

        // increments
        if (this.type == 1) {
            _return = this.generateIncrementResults(_data['reps'], _data['increments']);
        }

        // timed
        if (this.type == 2) {
            Duration _duration = _data['duration'];
            _return = {'time': _duration.inMilliseconds};
        }

        if (this.type == 3 || this.type == 4) {
            num _value = _data['value'];
            _value    += _data['increment'];
            if (_value < 0) _value = 0;
            if (_value == null) _value = 0;
            _return = {'value': _value};
        }

        return _return;

    }

    /// Work out if supplied increments are greater than the event allow
    /// if so, add them to the main rep result
    /// if not, send it back
    Map generateIncrementResults(_reps, _increments) {

        Map _return = {"reps": _reps, "increments": _increments};

        // if we're taking away increments and it goes below 0
        if (_increments < 0) {

            // don't go lower than 0 main and 0 increments
            if (_reps <= 0 && _increments <= 0) {
                _increments = 0;
            }

            // otherwise start at the top of the allowed increments
            // and remove a main
            else {
                _increments = this.increments - 1;
                _reps -= 1;
            }

        }
        else if (_increments >= this.increments) {

            // find out how many rep results these increments represent
            int _repResults = (_increments / this.increments).floor();

            // remove this many increments from the supplied increments
            _increments -= _repResults * this.increments;

            // add the reps
            _reps += _repResults;

        }

        if (_reps < 0) _reps = 0;

        _return["reps"] = _reps;
        _return["increments"] = _increments;

        return _return;

    }

}