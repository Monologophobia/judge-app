import 'package:flutter/material.dart';
import 'event.dart';

class Team {

    int id;
    int heatId;
    String leaderboardId;
    String name;
    String selectName;
    var controls;

    List<Result> results;
    Stopwatch stopwatch;

    // constructor
    Team(int id, int heatId, String leaderboardId, String name) {
        this.id = id;
        this.heatId = heatId;
        this.leaderboardId = leaderboardId;
        this.name = name;
        this.selectName = this.leaderboardId + ': ' + this.name;
        this.results = [];
        this.stopwatch = new Stopwatch();
    }

    Result getResultsByEvent(Event _event) {
        for (Result _result in this.results) {
            if (_result.eventId == _event.id) {
                // if a timed event has an existing time, recreate the stopwatch with this time
                if (_event.type == 2) this.stopwatch = new Stopwatch();
                return _result;
            }
        }
        Map _data;
        // increments or set increments
        if (_event.type == 1 || _event.type == 4) _data = {'reps': 0, 'increments': 0};
        // timed
        if (_event.type == 2) _data = {'time': 0};
        // multiple tallies
        if (_event.type == 3) {
            _data = {};
            _event.stages.forEach((key, value) => _data[key] = 0);
        }
        // set increments. dealt with above
        if (_event.type == 4) {}
        // ladder
        if (_event.type == 5) _data = {'ladder': 0, 'increments': 0};
        if (_event.type == 6) _data = {'laps': 0};
        return Result(_event.id, _data, null);
    }

    /// Set the result for a team by event
    void setResultsByEvent(Event _event, Map _data, DateTime _time) {

        Map _result;
        // increments or set increments
        if (_event.type == 1 || _event.type == 4) _result = {'reps': _data['reps'], 'increments': _data['increments']};
        // timed
        if (_event.type == 2) _result = {'time': _data['time']};
        // multiple tallies
        if (_event.type == 3) {
            _result = {};
            _event.stages.forEach((index, name) {
                int _key = (index is int ? index : int.parse(index));
                _result[_key] = (_data.containsKey(_key) ? _data[_key] : 0);
            });
        }

        // set increments. dealt with above
        if (_event.type == 4) {}
        // ladder
        if (_event.type == 5) _result = {'ladder': _data['ladder'], 'increments': _data['increments']};
        if (_event.type == 6) _result = {'laps': _data['laps']};

        Result _eventResult = Result(_event.id, _result, null);

        // check if the result for this event already exists. If it does, delete it.
        for (Result _result in this.results) {
            if (_result.eventId == _event.id) {
                this.results.remove(_result);
                break;
            }
        }
        // add the new result
        this.results.add(_eventResult);

    }

    void formatIncomingNetworkResults(Map _meta, Event _event) {
        Map _data = {};
        if (_event.type == 1) _data = {'reps': int.parse(_meta['reps']), 'increments': int.parse(_meta['increments'])};
        if (_event.type == 2) _data = {'time': _meta['milliseconds']};
        if (_event.type == 3) {
            for (Map _tally in _meta['tallies']) {
                _event.stages.forEach((key, value) {
                    int _index = (_tally['index'] is int ? _tally['index'] : int.parse(_tally['index']));
                    int _key = (key is int ? key : int.parse(key));
                    if (_key == _index) {
                        num _value = (_tally['result'] is num ? _tally['result'] : num.parse(_tally['result']));
                        _data[_key] = _value;
                    }
                });
            }
        }
        if (_event.type == 4) {
            int _reps = (_meta['reps'] is int ? _meta['reps'] : int.parse(_meta['reps']));
            int _increments = (_meta['setincrements'] is int ? _meta['setincrements'] : int.parse(_meta['setincrements']));
            _data = {'reps': _reps, 'increments': _increments};
        }
        if (_event.type == 5) {
            int _ladder = _meta['ladder']['index'];
            int _increments = _meta['increments']['index'];
            _data = {'ladder': _ladder, 'increments': _increments};
        }
        if (_event.type == 6) {
            num _laps = (_meta['laps'] is num ? _meta['laps'] : num.parse(_meta['laps']));
            _data = {'laps': _laps};
        }
        this.setResultsByEvent(_event, _data, null);
    }

    Map formatOutgoingNetworkResults(Event _event) {
        // get only the result we are submitting
        Result _result;
        for (Result _r in this.results) {
            if (_r.eventId == _event.id) {
                _result = _r;
                break;
            }
        }
        Map _formattedResult = {};
        if (_event.type == 0) {
            _formattedResult['total'] = _result.data['reps'];
        }
        if (_event.type == 1) {
            _formattedResult['reps'] = _result.data['reps'];
            _formattedResult['increments'] = _result.data['increments'];
        }
        if (_event.type == 2) {
            _formattedResult['milliseconds'] = _result.data['time'];
        }
        if (_event.type == 3) {
            List _list = [];
            _result.data.forEach((key, value) {
                Map _map = {'index': key, 'result': value};
                _list.add(_map);
            });
            _formattedResult['tallies'] = _list;
        }
        if (_event.type == 4) {
            _formattedResult['reps'] = _result.data['reps'];
            _formattedResult['setincrements'] = _result.data['increments'];
        }
        if (_event.type == 5) {
            _formattedResult['ladder'] = _result.data['ladder'];
            _formattedResult['ladderincrements'] = _result.data['increments'];
        }
        if (_event.type == 6) {
            _formattedResult['laps'] = _result.data['laps'];
        }
        return _formattedResult;
    }

}

class Result {

    int eventId;
    Map data;
    DateTime updatedAt;

    Result(this.eventId, this.data, this.updatedAt);

}