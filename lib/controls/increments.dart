import 'package:flutter/material.dart';
import 'customiconbutton.dart';
import '../models/team.dart';
import '../models/event.dart';

class IncrementControls extends StatefulWidget {
    final Team team;
    final Result results;
    final Event event;
    IncrementControls({Key key, this.team, this.results, this.event}): super(key: key);
    @override
    _IncrementControlsState createState() => new _IncrementControlsState();
}

class _IncrementControlsState extends State<IncrementControls> {

    int reps = 0;
    int increments = 0;

    @override
    void initState() {
        this.reps = widget.results.data['reps'];
        this.increments = widget.results.data['increments'];
        super.initState();
    }

    @override
    Widget build(BuildContext context) {
        return Row(
            children: [
                // main reps
                Container(
                    width: 100.0,
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [

                            // main subtraction button
                            CustomIconButton(icon: Icon(Icons.remove_circle, color: Colors.blueGrey), callback: () => incrementResult(-1, 0)),

                            // main text
                            Container(
                                width: 50.0,
                                child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                        new Text(this.reps.toString()),
                                        SingleChildScrollView(
                                            scrollDirection: Axis.horizontal,
                                            child: new Text(widget.event.mainText, style: TextStyle(color: Colors.grey, fontSize: 10.0))
                                        ),
                                    ]
                                ),
                            ),

                            // main addition
                            CustomIconButton(icon: Icon(Icons.add_circle, color: Colors.blueGrey), callback: () => incrementResult(1, 0)),

                        ]
                    )
                ),

                Padding(padding: EdgeInsets.only(left: 10.0),),

                // increments
                Container(
                    width: 100.0,
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [

                            // increments subtract
                            CustomIconButton(icon: Icon(Icons.remove_circle, color: Colors.blueGrey), callback: () => incrementResult(0, -1)),

                            // increments text
                            Container(
                                width: 50.0,
                                child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                        new Text(this.increments.toString()),
                                        SingleChildScrollView(
                                            scrollDirection: Axis.horizontal,
                                            child: new Text(widget.event.incrementText, style: TextStyle(color: Colors.grey, fontSize: 10.0)),
                                        ),
                                    ]
                                ),
                            ),

                            // increments add
                            CustomIconButton(icon: Icon(Icons.add_circle, color: Colors.blueGrey), callback: () => incrementResult(0, 1))
                        ]
                    )
                )
            ]
        );
    }

    void incrementResult(reps, increments) {
        Map _data    = {'reps': this.reps + reps, 'increments': this.increments + increments};
        Map _results = widget.event.generateResults(_data);
        setState(() {
            this.reps = _results['reps'];
            this.increments = _results['increments'];
            widget.results.data['reps'] = this.reps;
            widget.results.data['increments'] = this.increments;
        });
        widget.team.setResultsByEvent(widget.event, {'reps': this.reps, 'increments': this.increments}, null);
    }

}