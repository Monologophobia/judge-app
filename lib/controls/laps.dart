import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../models/team.dart';
import '../models/event.dart';

class LapControls extends StatefulWidget {

    final Team team;
    final Result results;
    final Event event;
    final ValueNotifier notifier;
    final GlobalKey globalKey = GlobalKey();
    final Function setParentState;
    LapControls({GlobalKey key, this.team, this.results, this.event, this.notifier, this.setParentState}): super(key: key);

    @override
    _LapControlsState createState() => _LapControlsState();

    get key => globalKey;

}

class _LapControlsState extends State<LapControls> {

    num laps = 0;

    @override
    void initState() {
        this.laps = widget.results.data['laps'];
        super.initState();
        widget.notifier.addListener(() {
            this.update(widget.notifier.value);
        });
    }

    @override
    Widget build(BuildContext context) {

        final TextEditingController _resultController = new TextEditingController.fromValue(
            new TextEditingValue(
                text: this.laps.toString(),
                selection: new TextSelection.collapsed(offset: this.laps.toString().length - 1)
            )
        );
        final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

        Widget _increment = Container();
        num _increments = (widget.event.increments is num ? widget.event.increments : num.parse(widget.event.increments));
        if (_increments != 1) {
            if (_increments == 0.25) {
                _increment = Container(
                    child: Row(
                        children: [
                            RaisedButton(
                                child: Text('+0.25', style: TextStyle(color: Colors.white70),),
                                color: Colors.black12,
                                onPressed: () => incrementResult(0.25)
                            ),
                            RaisedButton(
                                child: Text('+0.5', style: TextStyle(color: Colors.white70),),
                                color: Colors.black12,
                                onPressed: () => incrementResult(0.5)
                            ),
                            RaisedButton(
                                child: Text('+0.75', style: TextStyle(color: Colors.white70),),
                                color: Colors.black12,
                                onPressed: () => incrementResult(0.75)
                            )
                        ]
                    )
                );
            }
            else {
                _increment = Container(
                    child: RaisedButton(
                        child: Text('+ ' + _increments.toString(), style: TextStyle(color: Colors.white70),),
                        color: Colors.black12,
                        onPressed: () => incrementResult(_increments)
                    )
                );
            }
        }

        return Container(
            child: Column(
                children: [
                    Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                            IconButton(
                                color: Colors.white,
                                onPressed: () => incrementResult(-1),
                                icon: Icon(Icons.remove_circle)
                            ),
                            FlatButton(
                                child: Row(
                                    children: <Widget>[
                                        Text(this.laps.toString(), style: TextStyle(fontSize: 20.0, color: Colors.white)),
                                        Padding(padding: EdgeInsets.only(left: 5.0),),
                                        Text(widget.event.mainText, style: TextStyle(fontSize: 20.0, color: Colors.white)),
                                    ],
                                ),
                                onPressed: () {
                                    showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                            return AlertDialog(
                                                title: Text('Update Result'),
                                                content: Container(
                                                    child: Column(
                                                        mainAxisSize: MainAxisSize.min,
                                                        children: [
                                                            Text('Please enter this team\'s result.'),
                                                            _buildResultInput(_formKey, _resultController)
                                                        ]
                                                    )
                                                ),
                                                actions: <Widget>[
                                                    FlatButton(
                                                        child: new Text('Cancel'),
                                                        onPressed: () {
                                                            Navigator.of(context).pop();
                                                        },
                                                    ),
                                                    FlatButton(
                                                        child: new Text('Update Result'),
                                                        onPressed: () {
                                                            if (_formKey.currentState.validate()) {
                                                                num _laps = setResult(num.parse(_resultController.text));
                                                                this.laps = _laps;
                                                                widget.results.data['laps'] = _laps;
                                                                update(_laps);
                                                                Navigator.of(context).pop();
                                                                // OK...
                                                                // so, there is a bug where if the user dismisses the keyboard
                                                                // it actually disposes the entire widget
                                                                // meaning all of the above gets overwritten by old data
                                                                // and doesn't update the UI as it's no longer mounted
                                                                // BUT the data has been written to the team, the control result, and
                                                                // the ui variable (but not ui refreshed)
                                                                // ui refresh makes the data persist
                                                                // as this widget is unmounted, we can't use that for setstate
                                                                // but what is mounted? THE PARENT
                                                                // fuck me I'm starting to hate flutter
                                                                widget.setParentState();
                                                            }
                                                        }
                                                    ),
                                                ],
                                            );
                                        },
                                    );
                                }
                            ),
                            IconButton(
                                color: Colors.white,
                                onPressed: () => incrementResult(1),
                                icon: Icon(Icons.add_circle),
                            )
                        ]
                    ),
                    _increment
                ]
            )
        );
    }

    Form _buildResultInput(GlobalKey _key, TextEditingController _controller) {
        return Form(
            key: _key,
            child: TextFormField(
                controller: _controller,
                key: new Key('result'),
                decoration: const InputDecoration(
                    border: const UnderlineInputBorder(),
                    labelText: 'Result',
                ),
                keyboardType: TextInputType.numberWithOptions(decimal: true),
                validator: (value) {
                    if (value.isEmpty) {
                        return 'Result is missing';
                    }
                    num _value;
                    try {
                        _value = num.parse(value);
                    }
                    catch (exception) {
                        return 'Result is not a valid number';
                    }
                    if (_value % this.widget.event.increments != 0) {
                        return 'Not a valid increment. Must be in ' + this.widget.event.increments.toString();
                    }
                    if (_value < 0) {
                        return 'Cannot be less than 0';
                    }
                }
            )
        );
    }

    void incrementResult(increments) {
        num _laps = this.laps + increments;
        update(_laps);
    }

    num setResult(num _laps) {
        Map _data = {'laps': _laps};
        if (_data['laps'] < 0) _data['laps'] = 0;
        Map _results = widget.event.generateResults(_data);
        widget.team.setResultsByEvent(widget.event, {'laps': _results['laps']}, null);
        return _results['laps'];
    }

    void update(num _result) {
        num _laps = setResult(_result);
        if (this.mounted) {
            setState(() {
                widget.results.data['laps'] = _laps;
                this.laps = _laps;
            });
        }
    }

}