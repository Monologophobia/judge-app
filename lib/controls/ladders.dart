import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/foundation.dart';
import 'customiconbutton.dart';
import '../models/team.dart';
import '../models/event.dart';

class LadderControls extends StatefulWidget {
    final Team team;
    final Result results;
    final Event event;
    LadderControls({Key key, this.team, this.results, this.event}): super(key: key);
    @override
    _LadderControlsState createState() => new _LadderControlsState();
}

class _LadderControlsState extends State<LadderControls> {

    int ladder;
    int increments = 0;
    List<Widget> incrementsRadioList = [];
    List<DropdownMenuItem> ladderDropdownList = [];

    @override
    void initState() {

        this.ladder = widget.event.ladder[0]['index'];
        for  (Map _ladder in widget.event.ladder) {
            if (_ladder['index'] == widget.results.data['ladder']) {
                this.ladder = widget.results.data['ladder'];
                break;
            }
        }

        this.increments = widget.results.data['increments'];

        this.ladderDropdownList = getLadderDropdownItems();

        super.initState();

    }

    @override
    Widget build(BuildContext context) {
        return Row(
            children: [
                // main reps
                CustomIconButton(icon: Icon(Icons.remove_circle, color: Colors.blueGrey), callback: () => incrementMain(-1)),
                Padding(padding: EdgeInsets.only(right: 10.0),),
                Column(
                    children: [
                        buildLadder(),
                        SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: new Text(widget.event.mainText, style: TextStyle(color: Colors.grey, fontSize: 10.0))
                        ),
                    ]
                ),
                Padding(padding: EdgeInsets.only(left: 10.0),),
                CustomIconButton(icon: Icon(Icons.add_circle, color: Colors.blueGrey), callback: () => incrementMain(1)),
                Padding(padding: EdgeInsets.only(left: 20.0),),
                // increments (if needed)
                buildFinalIncrements()
            ]
        );
    }

    Widget buildLadder() {
        return DropdownButton(
            value: this.ladder,
            items: this.ladderDropdownList,
            onChanged: ((_value) {
                changedLadderDropDownItem(_value);
            })
        );
    }

    List<DropdownMenuItem> getLadderDropdownItems() {

        // create object from map
        List<DropdownMenuItem> _items = [];
        for (Map _ladder in widget.event.ladder) {
            _items.add(DropdownMenuItem(
                value: _ladder['index'],
                child: Text(_ladder['name'])
            ));
        }

        return _items;

    }

    Widget buildFinalIncrements() {
        // if not needed, send a blank widget
        if (this.widget.event.increments.length <= 0) {
            return Container();
        }
        return Container(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                    Container(
                        alignment: Alignment(0, 0),
                        child: SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: buildIncrementRadios()
                            /*Row(
                                children: buildIncrementRadios()
                            ),*/
                        ),
                    ),
                    SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: new Text(widget.event.incrementText, style: TextStyle(color: Colors.grey, fontSize: 10.0))
                    ),
                ]
            ),
        );
    }

    DropdownButton buildIncrementRadios() {

        List<DropdownMenuItem> _radios = [];

        for (var _increment in widget.event.increments) {

            _radios.add(DropdownMenuItem(
                value: _increment['index'],
                child: Text(_increment['name'])
            ));

            /*_radios.add(
                Column(
                    children: <Widget>[
                        SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Text(_increment['name'].toString(), style: TextStyle(color: Colors.grey, fontSize: 10.0)),
                        ),
                        Container(
                            width: 30,
                            height: 30,
                            child: Radio<int>(
                                value: _increment['index'],
                                groupValue: this.increments,
                                onChanged: handleIncrementRadioValueChanged
                            ),
                        )
                    ],
                )
            );*/
        }

        //return _radios;

        return DropdownButton(
            value: this.increments,
            items: _radios,
            onChanged: ((_value) {
                this.handleIncrementRadioValueChanged(_value);
            })
        );

    }

    void handleIncrementRadioValueChanged(int value) {
        setState(() {
            this.increments = value;
            widget.results.data['increments'] = this.increments;
        });
        widget.team.setResultsByEvent(widget.event, {'ladder': this.ladder, 'increments': this.increments}, null);
    }

    void incrementMain(int _value) {
        int _newValue = this.ladder + _value;
        for (Map _ladder in widget.event.ladder) {
            if (_ladder['index'] == _newValue) {
                changedLadderDropDownItem(_newValue);
                break;
            }
        }
    }

    void changedLadderDropDownItem(int _value) {
        setState(() {
            this.ladder = _value;
            widget.results.data['ladder'] = this.ladder;
        });
        widget.team.setResultsByEvent(widget.event, {'ladder': this.ladder, 'increments': this.increments}, null);
    }

}