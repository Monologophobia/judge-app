import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/foundation.dart';
import 'customiconbutton.dart';
import '../models/team.dart';
import '../models/event.dart';

class SetIncrementsControls extends StatefulWidget {
    final Team team;
    final Result results;
    final Event event;
    SetIncrementsControls({Key key, this.team, this.results, this.event}): super(key: key);
    @override
    _SetIncrementsState createState() => new _SetIncrementsState();
}

class _SetIncrementsState extends State<SetIncrementsControls> {

    double percentage = 0;
    int reps = 0;
    List<Widget> incrementsRadioList = [];
    int increments = 0;

    @override
    void initState() {

        this.reps       = widget.results.data['reps'];
        this.increments = widget.results.data['increments'];

        super.initState();

    }

    @override
    Widget build(BuildContext context) {
        return Row(
            children: [
                // main reps
                Container(
                    width: 110.0,
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [

                            // main subtraction button
                            CustomIconButton(icon: Icon(Icons.remove_circle, color: Colors.blueGrey), callback: () => incrementReps(-1)),

                            // main text
                            Container(
                                child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                        new Text(this.reps.toString()),
                                        SingleChildScrollView(
                                            padding: EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                                            scrollDirection: Axis.horizontal,
                                            child: new Text(widget.event.mainText, style: TextStyle(color: Colors.grey, fontSize: 10.0))
                                        ),
                                    ]
                                ),
                            ),

                            // main addition
                            CustomIconButton(icon: Icon(Icons.add_circle, color: Colors.blueGrey), callback: () => incrementReps(1)),

                        ]
                    )
                ),

                Padding(padding: EdgeInsets.only(left: 10.0),),

                // increments
                Container(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                            Container(
                                alignment: Alignment(0, 0),
                                child: SingleChildScrollView(
                                    scrollDirection: Axis.horizontal,
                                    child: Row(
                                        children: buildStageRadios()
                                    ),
                                ),
                            ),
                            SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: new Text(widget.event.incrementText, style: TextStyle(color: Colors.grey, fontSize: 10.0))
                            ),
                        ]
                    ),
                )
            ]
        );
    }

    List<Widget> buildStageRadios() {

        List<Widget> _radios = [];
        for (int _increment in widget.event.increments) {
            _radios.add(
                Column(
                    children: <Widget>[
                        Text(_increment.toString(), style: TextStyle(color: Colors.grey, fontSize: 10.0)),
                        Container(
                            width: 30,
                            height: 30,
                            child: Radio<int>(
                                value: _increment,
                                groupValue: this.increments,
                                onChanged: handleRadioValueChanged
                            ),
                        )
                    ],
                )
            );
        }
        return _radios;
    }

    void handleRadioValueChanged(int value) {
        setState(() {
            this.increments = value;
            widget.results.data['increments'] = this.increments;
        });
        widget.team.setResultsByEvent(widget.event, {'reps': this.reps, 'increments': this.increments}, null);
    }

    void incrementReps(reps) {
        Map _results = widget.event.generateResults({'value': this.reps, 'increment': reps});
        setState(() {
            this.reps = _results['value'];
            widget.results.data['reps'] = this.reps;
        });
        widget.team.setResultsByEvent(widget.event, {'reps': this.reps, 'increments': this.increments}, null);
    }

}