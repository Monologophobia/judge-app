import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/foundation.dart';
import 'customiconbutton.dart';
import '../models/team.dart';
import '../models/event.dart';

class TallyControls extends StatefulWidget {
    final Team team;
    final Result results;
    final Event event;
    final ValueNotifier notifier;
    final GlobalKey globalKey = GlobalKey();
    final Function setParentState;
    TallyControls({Key key, this.team, this.results, this.event, this.notifier, this.setParentState}): super(key: key);
    @override
    _TallyControlsState createState() => new _TallyControlsState();
    get key => globalKey;
}

class _TallyControlsState extends State<TallyControls> {

    List<Map> tallies;

    @override
    void initState() {

        this.tallies = [];

        widget.results.data.forEach((key, value) {
            int _key   = (key is int ? key : int.parse(key));
            num _value = (value is num ? value : num.parse(value));
            this.tallies.add({'index': _key, 'value': _value});
        });

        widget.notifier.addListener(() {
            if (widget.notifier.value != null) {
                num index = widget.notifier.value;
                for (Map _tally in this.tallies) {
                    if (_tally['index'] == index) {
                        this.incrementResult(_tally, 1);
                        break;
                    }
                }
            }
        });

        super.initState();

    }

    List<Widget> buildStages() {
        List<Widget> _widgets = [];
        for (Map _tally in this.tallies) {
            String _stageName = this.getStageName(_tally['index']);
            _widgets.add(this.buildStage(_tally, _stageName));
            _widgets.add(Padding(padding: EdgeInsets.only(left: 0.0),));
        }
        return _widgets;
    }

    String getStageName(key) {
        String _name = 'Not Found';
        widget.event.stages.forEach((_key, _value) {
            if (_key == key) {
                _name = _value;
            }
        });
        return _name;
    }

    Widget buildStage(Map _tally, String _stageName) {
        return Container(
            child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: buildControls(_tally, _stageName),
            )
        );
    }

    List<Widget> buildControls(Map _tally, String _stageName) {
        num _tallyIncrement = 0.2;
        if (_tally['index'] == 1) _tallyIncrement = 0.5;
        final TextEditingController _resultController = new TextEditingController.fromValue(
            new TextEditingValue(
                text: _tally['value'].toString(),
                selection: new TextSelection.collapsed(offset: _tally['value'].toString().length - 1)
            )
        );
        final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
        return [
            Column(
                children: <Widget>[
                    Row(
                        children: [
                            IconButton(
                                color: Colors.white,
                                onPressed: () => incrementResult(_tally, -1),
                                icon: Icon(Icons.remove_circle)
                            ),
                            FlatButton(
                                padding: EdgeInsets.all(0.0),
                                child: Container(
                                    width: 90.0,
                                    alignment: Alignment.center,
                                    child: SingleChildScrollView(
                                        scrollDirection: Axis.horizontal,
                                        child: Row(
                                            children: [
                                                Text(_tally['value'].toString(), style: TextStyle(fontSize: 14.0, color: Colors.white)),
                                                Padding(padding: EdgeInsets.only(left: 5.0),),
                                                Text(_stageName, style: TextStyle(fontSize: 14.0, color: Colors.white)),
                                            ],
                                        ),
                                    )
                                ),
                                onPressed:() {
                                    showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                            return AlertDialog(
                                                title: Text('Update ' + _stageName + ' Result'),
                                                content: Container(
                                                    child: Column(
                                                        mainAxisSize: MainAxisSize.min,
                                                        children: [
                                                            Text('Please enter this team\'s ' + _stageName + ' result.'),
                                                            _buildResultInput(_formKey, _resultController)
                                                        ]
                                                    )
                                                ),
                                                actions: <Widget>[
                                                    FlatButton(
                                                        child: new Text('Cancel'),
                                                        onPressed: () {
                                                            Navigator.of(context).pop();
                                                        },
                                                    ),
                                                    FlatButton(
                                                        child: new Text('Update Result'),
                                                        onPressed: () {
                                                            if (_formKey.currentState.validate()) {
                                                                update(_tally, num.parse(_resultController.text));
                                                                Navigator.of(context).pop();
                                                                // see laps.dart at this similar location for explanation
                                                                widget.setParentState();
                                                            }
                                                        }
                                                    ),
                                                ],
                                            );
                                        },
                                    );
                                }
                            ),
                            IconButton(
                                color: Colors.white,
                                onPressed: () => incrementResult(_tally, 1),
                                icon: Icon(Icons.add_circle)
                            ),
                        ]
                    ),
                    RaisedButton(
                        child: Text('+' + _tallyIncrement.toString(), style: TextStyle(color: Colors.white70),),
                        color: Colors.black12,
                        onPressed: () => incrementResult(_tally, _tallyIncrement)
                    )
                ]
            ),
        ];
    }

    Form _buildResultInput(GlobalKey _key, TextEditingController _controller) {
        return Form(
            key: _key,
            child: TextFormField(
                controller: _controller,
                key: new Key('result'),
                decoration: const InputDecoration(
                    border: const UnderlineInputBorder(),
                    labelText: 'Result',
                ),
                keyboardType: TextInputType.numberWithOptions(decimal: true),
                validator: (value) {
                    if (value.isEmpty) {
                        return 'Result is missing';
                    }
                    num _value;
                    try {
                        _value = num.parse(value);
                    }
                    catch (exception) {
                        return 'Result is not a valid number';
                    }
                    // can't modulo a double
                    int _tempValue = (_value * 10).round();
                    if (_tempValue % 2 != 0 && _tempValue % 5 != 0) {
                        return 'Not a valid increment. Must be in 0.2 or 0.5';
                    }
                    if (_value < 0) {
                        return 'Cannot be less than 0';
                    }
                }
            )
        );
    }

    @override
    Widget build(BuildContext context) {
        return Row(
            children: this.buildStages()
        );
    }

    void incrementResult(Map _tally, num increment) {
        num _number = _tally['value'] + increment;
        _number = num.parse(_number.toStringAsFixed(2));
        update(_tally, _number);
    }

    num setResult(Map _tally, num _number) {

        if (_number < 0) _number = 0;

        // set new tally score
        _tally['value'] = _number;

        // overwrite this.tallies
        for (Map _t in this.tallies) {
            if (_t['index'] == _tally['index']) {
                _t = _tally;
            }
        }

        // overwrite team score
        Map _result = {};
        for (Map _t in this.tallies) {
            _result[_t['index']] = _t['value'];
        }
        widget.team.setResultsByEvent(widget.event, _result, null);

        // overwrite widget.results.data
        widget.results.data.forEach((key, value) {
            if (key == _tally['index']) {
                widget.results.data[_tally['index']] = _tally['value'];
            }
        });

        return _number;

    }

    void update(Map _tally, num _number) {
        num _result = setResult(_tally, _number);
        if (this.mounted) {
            setState(() {
                //widget.results.data['laps'] = _laps;
                //this.laps = _laps;
            });
        }
    }

}