import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CustomIconButton extends StatelessWidget {

    const CustomIconButton({
        Key key,
        this.icon,
        this.callback,
    }) : super(key: key);

    final Icon icon;
    final VoidCallback callback;

    @override
    Widget build(BuildContext context) {
        return Container(
            width: 30.0,
            height: 30.0,
            child: IconButton(
                padding: new EdgeInsets.all(0.0),
                iconSize: 30.0,
                icon: this.icon,
                onPressed: this.callback
            ),
        );
    }

}