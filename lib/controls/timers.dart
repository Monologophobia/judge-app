import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/foundation.dart';
import 'customiconbutton.dart';
import '../models/team.dart';
import '../models/event.dart';

class TimerControls extends StatefulWidget {
    final Team team;
    final Result results;
    final Event event;
    TimerControls({Key key, this.team, this.results, this.event}): super(key: key);
    @override
    _TimerControlsState createState() => new _TimerControlsState();
}

class HoursMinutesSeconds extends StatefulWidget {
    final ValueListenable<int> notifier;
    HoursMinutesSeconds({Key key, this.notifier}): super(key: key);
    @override
    _HoursMinutesSecondsState createState() => new _HoursMinutesSecondsState();
}

class _HoursMinutesSecondsState extends State<HoursMinutesSeconds> {
    Widget build(BuildContext context) {
        String _text = widget.notifier.value.toString().padLeft(2, '0');
        return Text(_text);
    }
}

class Milliseconds extends StatefulWidget {
    final ValueListenable<int> notifier;
    Milliseconds({Key key, this.notifier}): super(key: key);
    @override
    _MillisecondsState createState() => new _MillisecondsState();
}

class _MillisecondsState extends State<Milliseconds> {
    Widget build(BuildContext context) {
        String _text = widget.notifier.value.toString().padLeft(3, '0');
        return Text(_text);
    }
}

class _TimerControlsState extends State<TimerControls> {

    Stopwatch stopwatch = new Stopwatch();

    int hours = 0;
    int minutes = 0;
    int seconds = 0;
    int milliseconds = 0;

    CustomIconButton toggleStopwatchActiveButton;

    ValueNotifier<int> minutesNotifier;
    ValueNotifier<int> secondsNotifier;
    ValueNotifier<int> millisecondsNotifier;

    Timer timer;
    Duration duration;

    @override
    void initState() {

        this.duration = Duration(milliseconds: widget.results.data['time']);
        calculateHoursMinutesSecondsAndMilliseconds(this.duration);

        IconData _icon = ((timer != null && timer.isActive) ? Icons.pause : Icons.play_arrow);
        toggleStopwatchActiveButton = CustomIconButton(icon: Icon(_icon, color: Colors.blueGrey), callback: () => this.toggleTimer());

        minutesNotifier = ValueNotifier(this.minutes);
        secondsNotifier = ValueNotifier(this.seconds);
        millisecondsNotifier = ValueNotifier(this.milliseconds);

        super.initState();

    }

    @override
    Widget build(BuildContext context) {
        return Container(
            child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                    this.toggleStopwatchActiveButton,
                    ValueListenableBuilder<int>(
                        valueListenable: this.minutesNotifier,
                        builder: (context, value, child) {
                            return HoursMinutesSeconds(notifier: this.minutesNotifier);
                        }
                    ),
                    Text(':'),
                    ValueListenableBuilder<int>(
                        valueListenable: this.secondsNotifier,
                        builder: (context, value, child) {
                            return HoursMinutesSeconds(notifier: this.secondsNotifier);
                        }
                    ),
                    Text('.'),
                    ValueListenableBuilder<int>(
                        valueListenable: this.millisecondsNotifier,
                        builder: (context, value, child) {
                            return Milliseconds(notifier: this.millisecondsNotifier);
                        }
                    ),
                    //CustomIconButton(icon: Icon(Icons.access_alarm, color: Colors.blueGrey), callback: () => this.doNothing())
                ]
            )
        );
    }

    void toggleTimer() {

        //if (timer != null && timer.isActive) timer.cancel();

        CustomIconButton _toggle;
        if (!this.stopwatch.isRunning) {
            this.stopwatch.start();
            _toggle = CustomIconButton(icon: Icon(Icons.pause, color: Colors.blueGrey), callback: () => toggleTimer());
            timer = new Timer.periodic(new Duration(milliseconds: 30), updateWidgets);
        }
        else {
            this.stopwatch.stop();
            _toggle = CustomIconButton(icon: Icon(Icons.play_arrow, color: Colors.blueGrey), callback: () => toggleTimer());
            widget.team.setResultsByEvent(widget.event, {'time': this.duration.inMilliseconds}, null);
        }
        setState(() {
            this.toggleStopwatchActiveButton = _toggle;
        });

    }

    void updateWidgets(Timer _timer) {
        this.duration = this.stopwatch.elapsed;
        calculateHoursMinutesSecondsAndMilliseconds(this.duration);
        minutesNotifier.value = this.minutes;
        secondsNotifier.value = this.seconds;
        millisecondsNotifier.value = this.milliseconds;
        
        widget.results.data['time'] = this.duration.inMilliseconds;
    }

    void calculateHoursMinutesSecondsAndMilliseconds(Duration _duration) {
        this.hours = _duration.inHours;
        this.minutes = _duration.inMinutes - (_duration.inHours * 60);
        this.seconds = _duration.inSeconds - (_duration.inMinutes * 60);
        this.milliseconds = _duration.inMilliseconds - (_duration.inSeconds * 1000);
    }

}