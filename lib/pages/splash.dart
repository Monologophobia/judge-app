import 'package:flutter/material.dart';

class SplashPage extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return new MaterialApp(
            title: 'Superhuman Sports',
            home: new MySplashPage(),
        );
    }
}

class MySplashPage extends StatefulWidget {
    @override
    _MySplashPageState createState() => new _MySplashPageState();
}

class _MySplashPageState extends State<MySplashPage> {
    @override
    Widget build(BuildContext context) {
        return new Scaffold(
            body: new Center(
                child: new Center(
                    child: new Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget> [
                            new Container(
                                padding: new EdgeInsets.fromLTRB(25.0, 20.0, 25.0, 0.0),
                                child: new Image.asset('assets/logo.png'),
                            ),
                            new Container(
                                padding: new EdgeInsets.all(25.0),
                            ),
                            new CircularProgressIndicator(),
                        ]
                    ),
                ),
            ),
        );
    }
}