import 'dart:io';
import 'dart:async';

import 'package:flutter/material.dart';

import '../helpers/routes.dart';
import '../helpers/exceptions.dart';

import '../main.dart';

class LoginPage extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return new MaterialApp(
            title: 'Login',
            home: new MyLoginPage(),
            routes: routes,
            theme: new ThemeData(
                primarySwatch: Colors.blueGrey
            )
        );
    }
}

class MyLoginPage extends StatefulWidget {
    @override
    _MyLoginPageState createState() => new _MyLoginPageState();
}

class _MyLoginPageState extends State<MyLoginPage> {

    // define controllers for the text fields
    final TextEditingController _email    = new TextEditingController();
    final TextEditingController _password = new TextEditingController();

    // Create a global key that will uniquely identify the Form widget and allow
    // us to validate the form
    final GlobalKey<ScaffoldState> _loginScaffoldKey = new GlobalKey<ScaffoldState>();
    final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

    Widget build(BuildContext context) {
        return new WillPopScope(
            onWillPop: _onWillPop,
            child: new Scaffold(
                key: _loginScaffoldKey,
                appBar: new AppBar(
                    title: new Text('Login'),
                ),
                body: SingleChildScrollView(
                    child: Column(
                        children: [
                            new Container(
                                width: 150.0,
                                padding: new EdgeInsets.fromLTRB(25.0, 40.0, 25.0, 10.0),
                                child: new Image.asset('assets/logo.png'),
                            ),
                            new Container(
                                padding: new EdgeInsets.fromLTRB(25.0, 20.0, 25.0, 0.0),
                                child: this._buildForm()
                            ),
                        ],
                    ),
                ),
            ),
        );
    }

    Form _buildForm() {
        return new Form(
            // assign unique id to form to allow us to track it
            key: _formKey,
            child: new Column(
                children: [

                    new TextFormField(
                        controller: _email,
                        key: new Key('username'),
                        decoration: const InputDecoration(
                            border: const UnderlineInputBorder(),
                            icon: const Icon(Icons.email),
                            hintText: 'Your Email Address',
                            labelText: 'Email *',
                        ),
                        keyboardType: TextInputType.emailAddress,
                        validator: (value) {
                            if (value.isEmpty) {
                                return 'Email is missing';
                            }
                            if (!value.contains('@')) {
                                return 'Not a valid email';
                            }
                        }
                    ),

                    const SizedBox(height: 24.0),

                    new TextFormField(
                        controller: _password,
                        key: new Key('password'),
                        decoration: const InputDecoration(
                            border: const UnderlineInputBorder(),
                            icon: const Icon(Icons.lock),
                            hintText: 'Your Password',
                            labelText: 'Password *',
                        ),
                        validator: (value) {
                            if (value.isEmpty) {
                                return 'Password is missing';
                            }
                            if (value.length < 3) {
                                return 'Password too short';
                            }
                        },
                        obscureText: true,
                    ),

                    const SizedBox(height: 32.0),

                    new RaisedButton(
                        color: Colors.blueGrey,
                        textColor: Colors.white,
                        key: new Key('login'),
                        onPressed: () {
                            _handleLogin();
                        },
                        child: new Text('LOGIN'),
                    )

                ]
            )
        );
    }

    Future<bool> _onWillPop() {
        exit(0);
    }

    void _handleLogin() async {

        if (_formKey.currentState.validate()) {

            _loginScaffoldKey.currentState.showSnackBar(
                new SnackBar(
                    duration: new Duration(seconds: 1),
                    content: new Row(
                        children: <Widget>[
                            new SizedBox(
                                width: 20.0,
                                height: 20.0,
                                child: new CircularProgressIndicator()
                            ),
                            new Padding(
                                padding: EdgeInsets.only(left: 16.0),
                                child: new Text("Logging in...")
                            ),
                        ],
                    ),
                )
            );

            try {
                await user.login(_email.text, _password.text);
                Navigator.pushNamed(context, '/judge');
            }
            catch (exception) {

                // get the right error message
                String _message;
                try {
                    _message = exception.cause;
                }
                on SocketException {
                    _message = "No Network Connection.";
                }
                on UserInvalid {
                    _message = "User credentials invalid.";
                }
                catch (e) {
                    _message = exception.toString();
                }

                // and create a new one
                showDialog (
                    context: context,
                    barrierDismissible: false,
                    builder: (BuildContext context) {
                        return new AlertDialog(
                            title: new Text('Error'),
                            content: new SingleChildScrollView(
                                child: new Text(_message),
                            ),
                            actions: <Widget>[
                                new FlatButton(
                                    child: new Text('Dismiss'),
                                    onPressed: () {
                                        Navigator.of(context).pop();
                                    },
                                ),
                            ],
                        );
                    },
                ).then<void>((v) { 
                    _loginScaffoldKey.currentState.hideCurrentSnackBar();
                });

            }

        }
    }

}
