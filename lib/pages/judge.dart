import 'package:path_provider/path_provider.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:share/share.dart';
import 'dart:convert';
import 'dart:io';

import '../main.dart';
import '../models/lane.dart';
import '../models/team.dart';
import '../models/heat.dart';
import '../models/event.dart';
import '../helpers/routes.dart';
import '../helpers/loading.dart';
import '../helpers/add_team.dart';
import '../helpers/submit.dart';
import '../controls/library.dart';

class JudgeApp extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return new MaterialApp(
            title: 'Superhuman Sports',
            routes: routes,
            theme: new ThemeData(
                primarySwatch: Colors.blueGrey
            ),
            home: new MyJudgeApp(title: 'Superhuman Sports'),
        );
    }
}

class MyJudgeApp extends StatefulWidget {
    MyJudgeApp({Key key, this.title}) : super(key: key);
    final title;
    _MyJudgeAppState createState() => new _MyJudgeAppState();
}

class _MyJudgeAppState extends State<MyJudgeApp> {

    List<Event> events = [];
    List<Lane> lanes   = [];
    List<Heat> heats   = [];

    Event selectedEvent;
    Heat selectedHeat;
    Text title;

    AddTeam addTeam;

    bool _firstLoad = true;

    final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

    @override
    void initState() {

        // build events from user login retrieval
        for (Map _event in user.events) {
            // TODO fix timed events
            // ignore timed events
            if (_event['type'] != 2) {
                this.events.add(
                    Event(_event['id'], _event['name'], _event['description'], _event['type'], _event['data'])
                );
            }
        }

        // build heats from user login retrieval
        for (Map _heat in user.heats) {
            this.heats.add(
                Heat(_heat['id'], _heat['name'])
            );
        }

        // generate lanes
        //for (int i = 1; i <= 20; i++) this.lanes.add(Lane(i, null));

        // create addteam helper
        // pass user teams and format there
        addTeam = AddTeam(user.teams, this.insertTeamIntoLane);

        this._synchroniseState();

        Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
            connectionChange(result);
        });

        super.initState();

    }

    void _synchroniseState() {
        if (this.selectedEvent == null) this.selectedEvent = events[0];
        if (this.selectedHeat == null) this.selectedHeat = heats[0];
        this.title = this._buildTitle();
    }

    Text _buildTitle() {
        String _text = this.selectedEvent.name + ': ';
        _text += this.selectedHeat.name;
        return new Text(_text);
    }

    @override
    Widget build(BuildContext context) {
        WidgetsBinding.instance.addPostFrameCallback((_) async => executeAfterWholeBuildProcess(context));
        return new Scaffold(
            key: this._scaffoldKey,
            appBar: new AppBar(
                title: this.title,
                actions: <Widget>[
                    new IconButton(
                        icon: new Icon(Icons.info_outline),
                        onPressed: () { this._showEventInfo(); }
                    ),
                    this._actionsMenu()
                ]
            ),
            body: new Center(
                child: new OrientationBuilder(
                    builder: (context, orientation) => _buildLaneList()
                ),
            ),
            floatingActionButton: _buildFloatingActions()
        );
    }

    Future<void> executeAfterWholeBuildProcess(BuildContext context) async {
        if (this._firstLoad) {
            this._showEvents();
            this._firstLoad = false;
        }
    }

    Widget _buildLaneList() {
        return ListView.builder(
            scrollDirection: Axis.vertical,
            itemBuilder: (context, index) {
                if (index < this.lanes.length) {
                    return _generateLaneRow(this.lanes[index]);
                }
                return _emptyLane();
            },
            itemCount: this.lanes.length + 1,
        );
    }

    Container _generateLaneRow(Lane _lane) {

        /*Color _laneColour = Color.fromARGB(255, 248, 177, 149);
        if (_lane.id % 2 == 0) _laneColour = Color.fromARGB(255, 246, 114, 128);
        if (_lane.id % 3 == 0) _laneColour = Color.fromARGB(255, 192, 108, 132);
        if (_lane.id % 4 == 0) _laneColour = Color.fromARGB(255, 108, 91, 123);
        if (_lane.id % 5 == 0) _laneColour = Color.fromARGB(255, 53, 92, 125);*/

        /*Color _laneColour = Color.fromARGB(255, 41, 103, 140);
        if (_lane.id % 2 == 0) _laneColour = Color.fromARGB(255, 246, 114, 128);
        if (_lane.id % 3 == 0) _laneColour = Color.fromARGB(255, 192, 108, 132);
        if (_lane.id % 4 == 0) _laneColour = Color.fromARGB(255, 108, 91, 123);
        if (_lane.id % 5 == 0) _laneColour = Color.fromARGB(255, 230, 140, 80);*/

        Color _laneColour = Color.fromARGB(255, 50, 50, 50);
        if (_lane.id % 2 == 0) _laneColour = Color.fromARGB(255, 75, 75, 75);
        if (_lane.id % 3 == 0) _laneColour = Color.fromARGB(255, 100, 100, 100);

        if (_lane.team == null) {
            //return _emptyLane(_colour, _laneColour, _lane);
        }

        Result _results = _lane.team.getResultsByEvent(this.selectedEvent);
        return Container(
            padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 10.0),
            color: _laneColour,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                    Row(
                        children: [
                            SingleChildScrollView(
                                child: _generateTeamName(_lane.team, _laneColour),
                            ),
                            _generateTeamControls(_lane, _laneColour)
                        ]
                    ),
                    _generateControls(_lane.team, _results),
                ],
            ),
        );

    }

    Container _emptyLane() {
        return Container(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                    Padding(padding: EdgeInsets.only(top: 10.0)),
                    Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                            //_laneText(_laneColour, _lane),
                            new RaisedButton(
                                child: Text(" Add Team"),
                                onPressed: () {
                                    addTeam.show(context);
                                }
                            ),
                            Padding(padding: EdgeInsets.only(right: 10.0),),
                            new RaisedButton(
                                child: Text(" Change Heat"),
                                onPressed: () {
                                    _changeHeat();
                                }
                            )
                        ],
                    ),
                    Padding(padding: EdgeInsets.only(bottom: 100.0)),
                ]
            ),
        );
    }

    Container _generateTeamName(Team _team, Color _colour) {
        return Container(
            width: MediaQuery.of(context).size.width - 100.0,

            padding: EdgeInsets.fromLTRB(16.0, 8.0, 8.0, 4.0),
            
            child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                        Text(_team.name, style: TextStyle(color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.w500)),
                        Text(" ID: " + _team.leaderboardId.toString(), style: TextStyle(fontSize: 10.0, color: Colors.white,))
                    ]
                )
            )
        );
    }

    void insertTeamIntoLane(Team _team, List<Map> _results) {
        // bug fix two lines
        // When adding teams, inputting results, and changing event, the results and the control results were persistent
        // reset results if they exist
        _team.results  = [];
        // reset controls if they exist.
        _team.controls = null;
        Lane _lane = Lane(this.lanes.length, _team.id);
        this.lanes.add(_lane);
        // first, format the results in to a useable format for the team
        for (Map _result in _results) {
            for (Event _event in this.events) {
                if (int.parse(_result['event_id']) == _event.id) {
                    _team.formatIncomingNetworkResults(_result['meta'], _event);
                }
            }
        }
        setState(() {
            _lane.team = _team;
        });
    }

    Widget _buildFloatingActions() {

        List<Widget> _widgets;

        // increments or set increments
        if (this.selectedEvent.type == 1 || this.selectedEvent.type == 4) {
            _widgets = [
                Container(
                    margin: const EdgeInsets.fromLTRB(0.0, 0.0, 12.5, 12.5),
                    width: 30.0,
                    height: 30.0,
                    child: FloatingActionButton(
                        heroTag: null,
                        child: Icon(Icons.remove,color: Colors.white,),
                        onPressed: () {
                            //for (Lane _lane in this.lanes) {}
                        },
                    ),
                ),
                FloatingActionButton(
                    heroTag: null,
                    child: Icon(Icons.add,color: Colors.white,),
                    onPressed: () {
                    },
                ),
            ];
            _widgets = [];
        }

        // timed
        if (this.selectedEvent.type == 2) {
            _widgets = [
                FloatingActionButton(
                    heroTag: null,
                    child: Icon(Icons.play_arrow,color: Colors.white,),
                    onPressed: () {
                        //startAllTimers();
                    },
                )
            ];
        }

        // multiple tallies
        if (this.selectedEvent.type == 3) {
            List<Widget> _buttons = [];
            this.selectedEvent.stages.forEach((key, value) {
                _buttons.add(
                    FloatingActionButton(
                        child: Text('+1 ' + value.toString(), style: TextStyle(fontWeight: FontWeight.normal, fontSize: 10.0), textAlign: TextAlign.center,),
                        onPressed: () {
                            for (Lane _lane in this.lanes) {
                                // If the controls initState has been called
                                if (_lane.team.controls != null) {
                                    // If the current state is valid
                                    if (_lane.team.controls.key.currentState != null) {
                                        // key might not have changed (multiple clicks on same button)
                                        _lane.team.controls.notifier.value = null;
                                        _lane.team.controls.notifier.value = key;
                                    }
                                    // else it's been disposed by listbuilder so update another way
                                    // we can't do it as if it's never been built as the control already exists
                                    // therefore update the control's results ready for the rebuild
                                    else {
                                        Map _results = {};
                                        _lane.team.controls.results.data.forEach((_k, _v) {
                                            _results[_k] = _v;
                                            if (_k == key) {
                                                _lane.team.controls.results.data[_k] += 1;
                                                _results[_k] = _lane.team.controls.results.data[_k];
                                            }
                                        });
                                        _lane.team.setResultsByEvent(this.selectedEvent, _results, null);
                                    }
                                }
                                // If it hasn't yet been initialised, we can update the result and use that as the start point
                                else {
                                    Result _results = _lane.team.getResultsByEvent(this.selectedEvent);
                                    _results.data[key] += 1;
                                    _lane.team.setResultsByEvent(this.selectedEvent, _results.data, null);
                                }
                            }
                        }
                    )
                );
                _buttons.add(
                    Padding(padding: EdgeInsets.only(left: 10.0),)
                );
            });
            _widgets = [
                Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: _buttons
                )
            ];
        }

        // Set Increments. Dealt with above
        if (this.selectedEvent.type == 4) { }

        // Ladder
        if (this.selectedEvent.type == 5) {
            _widgets = [];
        }

        if (this.selectedEvent.type == 6) {
            _widgets = [
                FloatingActionButton(
                    heroTag: null,
                    child: Icon(Icons.plus_one, color: Colors.white,),
                    onPressed: () {
                        for (Lane _lane in this.lanes) {
                            // If the controls initState has been called
                            if (_lane.team.controls != null) {
                                // If the current state is valid
                                if (_lane.team.controls.key.currentState != null) {
                                    // intermittent bug where if a decrement of a single lap had taken place, the FAB no longer worked
                                    // for that particular lane. Setting the notifier value to what it should be first seems to fix this
                                    _lane.team.controls.notifier.value = _lane.team.controls.results.data['laps'];
                                    _lane.team.controls.notifier.value = _lane.team.controls.results.data['laps'] + 1;
                                }
                                // else it's been disposed by listbuilder so update another way
                                // we can't do it as if it's never been built as the control already exists
                                // therefore update the control's results ready for the rebuild
                                else {
                                    _lane.team.controls.results.data['laps'] += 1;
                                    // in addition to setting the team controls data, set the team result as well
                                    _lane.team.setResultsByEvent(this.selectedEvent, {'laps': _lane.team.controls.results.data['laps']}, null);
                                }
                            }
                            // If it hasn't yet been initialised, we can update the result and use that as the start point
                            else {
                                Result _results = _lane.team.getResultsByEvent(this.selectedEvent);
                                _results.data['laps'] += 1;
                                _lane.team.setResultsByEvent(this.selectedEvent, _results.data, null);
                            }
                        }
                    },
                )
            ];
        }

        return Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.end,
            children: _widgets
        );

    }

    Widget _generateControls(Team _team, Result _results) {

        Widget _widget;

        // increments
        if (this.selectedEvent.type == 1) {
            _widget = new IncrementControls(team: _team, results: _results, event: this.selectedEvent);
        }

        // timed
        if (this.selectedEvent.type == 2) {
            _widget = new TimerControls(team: _team, results: _results, event: this.selectedEvent);
        }

        // multiple tallies
        if (this.selectedEvent.type == 3) {
            _widget = new TallyControls(team: _team, results: _results, event: this.selectedEvent, notifier: ValueNotifier(_results.data), setParentState: setParentState);
        }

        if (this.selectedEvent.type == 4) {
            _widget = new SetIncrementsControls(team: _team, results: _results, event: this.selectedEvent);
        }

        if (this.selectedEvent.type == 5) {
            _widget = new LadderControls(team: _team, results: _results, event: this.selectedEvent);
        }

        if (this.selectedEvent.type == 6) {
            _widget = new LapControls(team: _team, results: _results, event: this.selectedEvent, notifier: ValueNotifier(_results.data['laps']), setParentState: setParentState);
        }

        _team.controls = _widget;

        return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
                Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                        _widget,
                    ]
                )
            ]
        );

    }

    void setParentState() {
        setState(() {});
    }

    Widget _generateTeamControls(Lane _lane, Color _laneColour) {
        return Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
                IconButton(
                    icon: Icon(Icons.close),
                    color: Colors.white,
                    onPressed: () => showDialog(
                        context: context,
                        builder: (BuildContext context) {
                            return AlertDialog(
                                title: Text('Remove Team'),
                                content: Text('Are you sure you want to remove this team?'),
                                actions: <Widget>[
                                    FlatButton(
                                        child: new Text('Cancel'),
                                        onPressed: () {
                                            Navigator.of(context).pop();
                                        },
                                    ),
                                    FlatButton(
                                        child: new Text('Remove'),
                                        onPressed: () {
                                            setState(() {
                                                this.lanes.remove(_lane);
                                            });
                                            Navigator.of(context).pop();
                                        }
                                    ),
                                ],
                            );
                        },
                    ) 
                ),
                IconButton(
                    icon: Icon(Icons.check),
                    color: Colors.white,
                    onPressed: () {
                        Submit _submit = Submit(team: _lane.team, event: this.selectedEvent, lane: _lane, callback: finishSubmit);
                        _submit.getSignature(context);
                    }
                )
            ],
        );
    }

    void finishSubmit(String _message, Lane _lane) {
        setState(() {
            this.lanes.remove(_lane);
        });
        this._scaffoldKey.currentState.showSnackBar(
            SnackBar(content: Text(_message), duration: Duration(seconds: 5)),
        );
    }

    void _showEventInfo() {
        showDialog(
            context: context,
            builder: (BuildContext context) {
                return new AlertDialog(
                    title: new Text('Event Info'),
                    content: new SingleChildScrollView(
                        child: new Container(
                            child: Html(data: this.selectedEvent.description)
                        ),
                    ),
                    actions: <Widget>[
                        new FlatButton(
                            child: new Text('Dismiss'),
                            onPressed: () {
                                Navigator.of(context).pop();
                            },
                        )
                    ],
                );
            },
        );
    }

    PopupMenuButton _actionsMenu() {
        return new PopupMenuButton<String>(
            onSelected: _menuActions,
            itemBuilder: (BuildContext context) {
                List<PopupMenuEntry<String>> _list = [];
                _list.add(
                    PopupMenuItem<String>(
                        value: 'heat',
                        child: Text('Change Heat')
                    )
                );
                _list.add(
                    PopupMenuItem<String>(
                        value: 'event',
                        child: Text('Change Event')
                    )
                );
                /*for (Event _event in this.events) {
                    _list.add(
                        PopupMenuItem<String>(
                            value: _event.id.toString(),
                            child: Text(_event.name)
                        )
                    );
                }*/
                _list.add(
                    PopupMenuItem<String>(
                        value: 'synchronise',
                        child: Text('Synchronise Data')
                    )
                );
                _list.add(
                    PopupMenuItem<String>(
                        value: 'export',
                        child: Text('Export Data')
                    )
                );
                _list.add(
                    PopupMenuItem<String>(
                        value: 'logout',
                        child: Text('Logout')
                    )
                );
                return _list;
            }
        );
    }

    _menuActions(String value) {
        /*if (value == 'view') {
            _showEventInfo();
        }*/
        if (value == 'event') {
            _showEvents();
        }
        else if (value == 'heat') {
            this._changeHeat();
        }
        else if (value == 'synchronise') {
            this._doManualSync();
        }
        else if (value == 'export') {
            this._doExport();
        }
        else if (value == 'logout') {
            this._logout();
        }
        else {
            for (Event _event in this.events) {
                if (_event.id == int.parse(value)) {
                    setState(() {
                        this.selectedEvent = _event;
                        this._synchroniseState();
                    });
                    break;
                }
            }
        }
    }

    void _changeHeat() {
        BuildContext _dialogContext;
        List<Widget> _widgets = [];
        for (Heat _heat in this.heats) {
            _widgets.add(
                ListTile(
                    dense: true,
                    title: Text(_heat.name),
                    //enabled: this.selectedHeat != _heat,
                    onTap: () {
                        this.lanes = [];
                        setState(() {
                            this.title = this._buildTitle();
                            this.selectedHeat = _heat;
                            this._synchroniseState();
                        });
                        Navigator.of(_dialogContext).pop();
                        _showTeamsByHeat();
                    }
                )
            );
        }
        showDialog(
            context: context,
            builder: (BuildContext _context) {
                _dialogContext = _context;
                return new AlertDialog(
                    title: new Text('Change Heat'),
                    content: new SingleChildScrollView(
                        child: new Column(
                            children: _widgets
                        ),
                    ),
                    actions: <Widget>[
                        new FlatButton(
                            child: new Text('Dismiss'),
                            onPressed: () {
                                Navigator.of(_dialogContext).pop();
                            },
                        )
                    ],
                );
            },
        );
    }

    void _showEvents() {
        showDialog(
            context: context,
            builder: (BuildContext _context) {
                return new AlertDialog(
                    title: new Text('Select Event'),
                    content: Container(
                        width: 300.0,
                        child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: events.length,
                            itemBuilder: (context, index) {
                                return ListTile(
                                    title: Text('${events[index].name}'),
                                    onTap: () {
                                        this.lanes = [];
                                        setState(() {
                                            this.selectedEvent = events[index];
                                            this._synchroniseState();
                                        });
                                        Navigator.of(_context).pop();
                                        this._changeHeat();
                                    }
                                );
                            },
                        ),
                    ),
                    actions: <Widget>[
                        new FlatButton(
                            child: new Text('Dismiss'),
                            onPressed: () {
                                Navigator.of(_context).pop();
                            },
                        ),
                    ],
                );
            },
        );
    }

    void _showTeamsByHeat() {

        BuildContext _buildContext;

        List<Team> _teams   = [];
        List<Team> _selectedTeams = [];
        for (Team _team in this.addTeam.teams) {
            if (_team.heatId == this.selectedHeat.id) {
                _teams.add(_team);
            }
        }

        Function selectTeams(Team _team, bool _selected) {
            if (_selected) {
                _selectedTeams.add(_team);
            }
            else {
                _selectedTeams.remove(_team);
            }
            return null;
        }

        ValueNotifier<bool> _loading = ValueNotifier(false);

        showDialog(
            context: context,
            builder: (BuildContext _context) {
                _buildContext = _context;
                return new AlertDialog(
                    title: new Text('Select Teams for ' + this.selectedHeat.name),
                    content: HeatTeamSelect(teams: _teams, callback: selectTeams),
                    actions: <Widget>[
                        ValueListenableBuilder<bool>(
                            valueListenable: _loading,
                            builder: (_buildContext, value, child) {
                                return Loading(notifier: _loading);
                            }
                        ),
                        new FlatButton(
                            child: new Text('Dismiss'),
                            onPressed: () {
                                Navigator.of(_buildContext).pop();
                            },
                        ),
                        new FlatButton(
                            child: Text('Add Teams'),
                            onPressed: () async {
                                _loading.value = true;
                                this.lanes = [];
                                for (Team _team in _selectedTeams) {
                                    await this.addTeam.selectTeam(_team.selectName);
                                    await this.addTeam.updateLane(null);
                                }
                                _loading.value = false;
                                Navigator.of(_buildContext).pop();
                            }
                        )
                    ],
                );
            },
        );

    }

    Future<Function> connectionChange(ConnectivityResult connectivityResult) async {
        if (connectivityResult != ConnectivityResult.none) {
            await SubmitStoredResults().run();
            this._scaffoldKey.currentState.showSnackBar(
                SnackBar(content: Text("Reconnected to Network. Results Synchronised."), duration: Duration(seconds: 5)),
            );
        }
        else {
            this._scaffoldKey.currentState.showSnackBar(
                SnackBar(content: Text("Lost Network Connection."), duration: Duration(seconds: 5)),
            );
        }
        return null;
    }

    void _logout() async {
        await user.logout();
        Navigator.of(context).pushNamed("/login");
    }

    void _doManualSync() async {
        var connectivityResult = await (Connectivity().checkConnectivity());
        if (connectivityResult == ConnectivityResult.none) {
            this._scaffoldKey.currentState.showSnackBar(
                SnackBar(content: Text("No Network. Can't Synchronise. Try later."), duration: Duration(seconds: 5)),
            );
        }
        else {
            // first calculate what we're uploading to display to user
            List<Map> _results = await database.queryAll('results');
            int _submitted = 0;
            int _notSubmitted = 0;
            for (Map _result in _results) {
                if (_result['submitted'] != null) {
                    _submitted++;
                }
                else {
                    _notSubmitted++;
                }
            }
            // do the synchronisation
            await SubmitStoredResults().run();
            String _message = _notSubmitted.toString() + " Results Submitted. " + (_notSubmitted + _submitted).toString() + " Total Results";
            this._scaffoldKey.currentState.showSnackBar(
                SnackBar(content: Text(_message), duration: Duration(seconds: 5)),
            );
        }
    }

    void _doExport() async {
        List<Map> _results = await database.queryAll('results');
        // generate a new list map as _results is directly linked to database rows
        List<Map> _data = [];
        for (Map _result in _results) {
            // remove signature
            Map _r = jsonDecode(_result['results']);
            Map<String, dynamic> _signatureRemoved = {
                'leaderboard_id': _r['leaderboard_id'],
                'team_name': _r['team_name'],
                'results': _r['results']
            };
            Map _d = {
                'team_id': _result['team_id'],
                'event_id': _result['event_id'],
                'results': _signatureRemoved,
                'submitted': _result['submitted'],
                'notes': _result['notes']
            };
            _data.add(_d);
        }
        Share.share(_data.toString()).catchError((onError) {
            // if fail, dump to a file
            _dumpExport(_results.toString());                                
        });
    }

    _dumpExport(String _content) async {
        final _directory = await getApplicationDocumentsDirectory();
        final _file = new File(_directory.path + "/export.json");
        _file.writeAsString(_content);
        showDialog(
            context: context,
            builder: (BuildContext context) {
                return new AlertDialog(
                    contentPadding: EdgeInsets.all(16.0),
                    content: new Column (
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                            new Text('File created.'),
                            new Padding(padding: EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0)),
                            new Text(
                                _file.path.toString(),
                                textAlign: TextAlign.center,
                                style: new TextStyle(fontSize: 10.0)
                            ),
                            new Padding(padding: EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0)),
                            new Text(
                                'Automatic sharing coming soon. Use a file explorer to retrieve file.',
                                textAlign: TextAlign.center,
                                style: new TextStyle(fontSize: 12.0)
                            )
                        ]
                    ),
                    actions: <Widget>[
                        new FlatButton(
                            child: new Text('Dismiss'),
                            onPressed: () {
                                Navigator.of(context).pop();
                            },
                        ),
                        new FlatButton(
                            child: new Text('Copy File Path'),
                            onPressed: () {
                                Clipboard.setData(new ClipboardData(text: _file.path.toString()));
                                Navigator.of(context).pop();
                            },
                        ),
                    ],
                );
            },
        );
    }

}