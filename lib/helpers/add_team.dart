import 'dart:collection';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

import '../models/lane.dart';
import '../models/team.dart';
import 'loading.dart';
import '../main.dart';

class AddTeam {

    List<Team> teams = [];
    Function callback;
    String _selectedTeam;
    ValueNotifier<bool> _loading;

    AddTeam(List<Map> _teams, Function _callback) {

        // build team list from user login data
        for (Map _team in _teams) {
            this.teams.add(
                Team(_team['id'], _team['heat_id'], _team['leaderboard_id'], _team['name'])
            );
        }

        this.callback = _callback;

        _loading = ValueNotifier(false);

    }

    void show(BuildContext context) {

        var _teamSelect = TeamSelect(teams: this.teams, callback: selectTeam);

        showDialog(
            context: context,
            builder: (BuildContext context) {
                return AlertDialog(
                    title: new Text('Add Team'),
                    content: Container(
                        height: 75.0,
                        child: Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                                Text("ID or Team Name"),
                                _teamSelect
                            ]
                        ),
                    ),
                    actions: <Widget>[
                        ValueListenableBuilder<bool>(
                            valueListenable: this._loading,
                            builder: (context, value, child) {
                                return Loading(notifier: this._loading);
                            }
                        ),
                        new FlatButton(
                            child: new Text('Dismiss'),
                            onPressed: () {
                                Navigator.of(context).pop();
                            },
                        ),
                        new FlatButton(
                            child: Text('Add Team'),
                            onPressed: () => updateLane(context)
                        )
                    ],
                );
            },
        );

    }

    Future<void> selectTeam(data) {
        this._selectedTeam = data;
        return null;
    }

    Future<void> updateLane(BuildContext context) async {

        if (this._selectedTeam == null) return null;
        // don't do more than once
        if (this._loading.value == true) return null;

        // get team by string
        Team _team;
        for (Team _t in this.teams) {
            if (this._selectedTeam == _t.selectName) {
                _team = _t;
                break;
            }
        }

        if (_team == null) return null;

        // get existing results from server
        List<Map> _results = await this.getResults(_team);

        if (context != null) Navigator.of(context).pop();

        // send results back
        this.callback(_team, _results);

    }

    Future<List<Map>> getResults(Team _team) async {
        List<Map> _list = [];
        try {
            this._loading.value = true;
            String _url = 'results/' + _team.id.toString();
            Map _results = await network.get(_url);
            if (_results['response'].length > 0) {
                for (Map _response in _results['response']) {
                    _list.add(_response);
                }
            }
            this._loading.value = false;
        }
        catch (exception) {
            this._loading.value = false;
        }
        return _list;
    }

}

class TeamSelect extends StatefulWidget {
    TeamSelect({Key key, this.teams, this.callback}) : super(key: key);
    final List teams;
    final Function callback;
    TeamSelectState createState() => new TeamSelectState();
}

class TeamSelectState extends State<TeamSelect> {

    @override
    Widget build(BuildContext context) {

        TextEditingController _controller = new TextEditingController();
        return TypeAheadField(
            textFieldConfiguration: TextFieldConfiguration(
                controller: _controller,
                autofocus: true
            ),
            suggestionsCallback: (pattern) {
                return this.getTeamsByPattern(pattern);
            },
            itemBuilder: (context, suggestion) {
                return ListTile(
                    title: Text(suggestion),
                );
            },
            onSuggestionSelected: (suggestion) {
                _controller.text = suggestion;
                widget.callback(suggestion);
            },
        );
    }

    List getTeamsByPattern(String _pattern) {

        String _searchTerm = _pattern.toLowerCase();

        List _found = [];
        for (Team _team in widget.teams) {
            if (_team.selectName.toLowerCase().contains(_searchTerm)) {
                _found.add(_team.selectName);
            }
        }

        return _found;

    }

}

class HeatTeamSelect extends StatefulWidget {
    HeatTeamSelect({Key key, this.teams, this.callback}) : super(key: key);
    final List teams;
    final Function callback;
    HeatTeamSelectState createState() => new HeatTeamSelectState();
}

class HeatTeamSelectState extends State<HeatTeamSelect> {

    HashMap<int, bool> _selectableTeams = HashMap();

    void initState() {
        for (Team _team in widget.teams) {
            _selectableTeams[_team.id] = false;
        }
        super.initState();
    }

    @override
    Widget build(BuildContext context) {

        return Scrollbar(
            child: SingleChildScrollView(
                child: new Column(
                    children: buildTeams(),
                )
            )
        );

    }

    List<Widget> buildTeams() {
        List<Widget> _widgets = [];

        for (Team _team in widget.teams) {

            _widgets.add(
                CheckboxListTile(
                    dense: true,
                    title: Text(_team.leaderboardId + ': ' + _team.name),
                    value: _selectableTeams[_team.id],
                    onChanged: (value) {
                        setState(() {
                            _selectableTeams[_team.id] = !_selectableTeams[_team.id];
                            value = _selectableTeams[_team.id];
                        });
                        widget.callback(_team, value);
                    },
                )
            );
        }
        return _widgets;
    }

}