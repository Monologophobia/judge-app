import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import '../models/event.dart';
import '../models/team.dart';
import '../models/lane.dart';
import 'loading.dart';
import '../main.dart';

class Submit {

    final Team team;
    final Event event;
    final Lane lane;
    final Function callback;
    final TextEditingController notes = TextEditingController();

    Submit({this.team, this.event, this.lane, this.callback});

    List<Offset> points;
    ValueNotifier<bool> _loading;

    void getSignature(BuildContext _context) {

        this.team.setResultsByEvent(this.event, this.team.controls.results.data, null);

        Signature _signature = Signature(callback: updateSignature);
        this._loading = ValueNotifier(false);

        Result _results = this.team.getResultsByEvent(this.event);
        Text _score = this.getResultsText(_results);

        showDialog(
            barrierDismissible: false,
            
            context: _context,
            builder: (BuildContext context) {
                return new AlertDialog(
                    title: new Text(this.team.name + ' - ' + event.name),
                    content: Container(
                        height: 375.0,
                        child: SingleChildScrollView(
                            child:
                        Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            _score,
                            TextField(
                                controller: this.notes,
                                decoration: const InputDecoration(
                                    border: const UnderlineInputBorder(),
                                    labelText: 'Leaderboard Notes',
                                ),
                            ),
                            Padding(padding: EdgeInsets.only(top: 15.0)),
                            Text("Team to sign below"),
                            Padding(padding: EdgeInsets.only(top: 2.0)),
                            _signature
                        ]
                    )
                        ),
                    ),
                    actions: <Widget>[
                        ValueListenableBuilder<bool>(
                            valueListenable: this._loading,
                            builder: (context, value, child) {
                                return Loading(notifier: this._loading);
                            }
                        ),
                        new FlatButton(
                            child: new Text('Cancel'),
                            onPressed: () {
                                Navigator.of(context).pop();
                            },
                        ),
                        new FlatButton(
                            child: Row(
                                children: [
                                    new Text('Submit Result'),
                                ]
                            ),
                            onPressed: () {
                                if (this.points != null && this.points.length > 0) {
                                    submitResult(context);
                                }
                                else {
                                    showDialog<void>(
                                        context: context,
                                        builder: (BuildContext context) {
                                            return AlertDialog(
                                                title: Text('Signature Required'),
                                                actions: <Widget>[
                                                    FlatButton(
                                                        child: Text('Dismiss'),
                                                        onPressed: () {
                                                            Navigator.of(context).pop();
                                                        },
                                                    ),
                                                ],
                                            );
                                        }
                                    );
                                }
                            }
                        )
                    ],
                );
            },
        );
    }

    void updateSignature(List<Offset> _points) {
        points = _points;
    }

    Text getResultsText(Result _results) {
        Text _score;
        // increments and setincrements
        if (this.event.type == 1 || this.event.type == 4) {
            _score = Text(
                _results.data['reps'].toString() +
                " " +
                event.mainText +
                "\r\n" +
                _results.data['increments'].toString() +
                " " +
                this.event.incrementText +
                "\r\n",
                style: TextStyle(fontSize: 20.0)
            );
        }
        // timer
        if (this.event.type == 2) {
            Duration _duration = new Duration(milliseconds: _results.data['time']);
            int _minutes      = _duration.inMinutes;
            int _seconds      = _duration.inSeconds - (_minutes * 60);
            int _milliseconds = _duration.inMilliseconds - (_minutes * 60 * 1000) - (_seconds * 1000);
            _score = Text(_minutes.toString().padLeft(2, "0") + ":" + _seconds.toString().padLeft(2, "0") + "." + _milliseconds.toString().padLeft(3, "0") + "\r\n", style: TextStyle(fontSize: 24.0),);
        }
        // tallies
        if (this.event.type == 3) {
            String _string = '';
            this.event.stages.forEach((_key, _name) {
                int key = (_key is int ? _key : int.parse(_key));
                _string += _results.data[key].toString() + " x " + _name + "\r\n";
            });
            _score = Text(_string, style: TextStyle(fontSize: 20.0));
        }
        // ladder
        if (this.event.type == 5) {
            String _string = '';
            for (Map _ladder in this.event.ladder) {
                if (_ladder['index'] == _results.data['ladder']) {
                    _string += _ladder['name'] + " " + this.event.mainText + "\r\n";
                    break;
                }
            }
            for (Map _increment in this.event.increments) {
                if (_increment['index'] == _results.data['increments']) {
                    _string += _increment['name'] + " " + this.event.incrementText + "\r\n";
                    break;
                }
            }
            _score = Text(_string, style: TextStyle(fontSize: 20.0));
        }
        if (this.event.type == 6) {
            _score = Text(_results.data['laps'].toString() + " " + this.event.mainText, style: TextStyle(fontSize: 20.0));
        }
        return _score;
    }

    void submitResult(BuildContext context) async {

        // if already submitting, do nothing
        if (this._loading.value == true) return null;

        this._loading.value = true;

        try {

            // generate payload and post
            Map _post = {
                'event_id': this.event.id,
                'team_id': this.team.id,
                'leaderboard_id': this.team.leaderboardId,
                'team_name': this.team.name,
                'signature': this.points.toString(),
                'results': this.team.formatOutgoingNetworkResults(this.event),
                'notes': this.notes.text.toString()
            };

            // Check if we have a network connection. If not, store it in the database for later upload
            var connectivityResult = await (Connectivity().checkConnectivity());
            String _text;
            if (connectivityResult == ConnectivityResult.none) {
                _text = 'No network available. Result will be uploaded later.';
                Map _data = _generateStoreData(_post, null);
                await database.insert('results', _data);
            }
            else {

                _text = 'Result Uploaded';
                await network.post('results', user, _post);

                // inset the record and mark as submitted
                Map _data = _generateStoreData(_post, DateTime.now().toString());
                await database.insert('results', _data);

                // if we have a network connection and we haven't thrown an exception yet,
                // try and upload everything that may have failed
                await SubmitStoredResults().run();

            }

            this._loading.value = false;

            Navigator.of(context).pop();

            this.callback(_text, this.lane);

        }
        catch (exception) {
            print(exception);
            this._loading.value = false;
        }
    }

    Map<String, dynamic> _generateStoreData(Map _result, String _submitted) {
        Map<String, dynamic> _data = {
            'team_id': this.team.id,
            'event_id': this.event.id,
            'results': jsonEncode(_result),
            'submitted': null,
            'notes': this.notes.text.toString()
        };
        return _data;
    }
}

class SubmitStoredResults {
    Future<void> run() async {
        var connectivityResult = await (Connectivity().checkConnectivity());
        if (connectivityResult != ConnectivityResult.none) {
            List<String> _columns = ['*'];
            List<Map> _results = await database.queryWhere('results', _columns, 'submitted IS NULL', []);
            String _now = DateTime.now().toString();
            if (_results.length > 0) {
                for (Map _result in _results) {

                    Map _r = jsonDecode(_result['results']);
                    Map _post = {
                        'event_id': _result['event_id'],
                        'team_id': _result['team_id'],
                        'leaderboard_id': _r['leaderboard_id'],
                        'team_name': _r['team_name'],
                        'signature': _r['signature'],
                        'results': _r['results'],
                        'notes': _result['notes']
                    };
                    await network.post('results', user, _post);

                    // mark record as submitted
                    Map<String, dynamic> _data = {'submitted': _now};
                    await database.update('results', 'id = ?', [_result['id']], _data);

                }
            }
        }
    }
}

class SignaturePainter extends CustomPainter {

    SignaturePainter(this.points);

    final List<Offset> points;

    void paint(Canvas canvas, Size size) {

        Paint paint = new Paint();
        paint.color = Colors.black;
        paint.strokeCap = StrokeCap.round;
        paint.strokeWidth = 5.0;

        for (int i = 0; i < points.length - 1; i++) {
            if (points[i] != null && points[i + 1] != null) {
                canvas.drawLine(points[i], points[i + 1], paint);
            }
        }

    }

    bool shouldRepaint(SignaturePainter other) => other.points != points;

}

class Signature extends StatefulWidget {
    final Function callback;
    Signature({this.callback});
    SignatureState createState() => new SignatureState();
}

class SignatureState extends State<Signature> {

    List<Offset> _points = <Offset>[];

    Widget build(BuildContext context) {
        return Container(
            height: 200.0,
            decoration: new BoxDecoration(
                color: Color.fromARGB(10, 0, 0, 0),
                border: new Border.all(color: Colors.black)
            ),
            child: Stack(
                children: [
                    GestureDetector(
                        onPanUpdate: (DragUpdateDetails details) {
                            RenderBox referenceBox = context.findRenderObject();
                            Offset localPosition = referenceBox.globalToLocal(details.globalPosition);
                            setState(() {
                                _points = new List.from(_points)..add(localPosition);
                                widget.callback(_points);
                            });
                        },
                        onPanEnd: (DragEndDetails details) {
                            _points.add(null);
                            widget.callback(_points);
                        }
                    ),
                    CustomPaint(painter: new SignaturePainter(_points))
                ],
            )
        );
    }

}