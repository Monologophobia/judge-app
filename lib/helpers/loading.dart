import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

class Loading extends StatefulWidget {
    Loading({Key key, this.notifier}) : super(key: key);
    final ValueListenable<bool> notifier;
    LoadingState createState() => new LoadingState();
}

class LoadingState extends State<Loading> {
    Widget build(BuildContext context) {
        return Visibility(
            visible: widget.notifier.value,
            child: Container(
                width: 20.0,
                height: 20.0,
                child: CircularProgressIndicator()
            )
        );
    }
}