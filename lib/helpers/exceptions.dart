class NoUser implements Exception {
    String cause;
    NoUser(this.cause);
}

class UserInvalid implements Exception {
    String cause;
    UserInvalid(this.cause);
}

class NetworkError implements Exception {
    String cause;
    NetworkError(this.cause);
}

class CredentialsInvalid implements Exception {
    String cause;
    CredentialsInvalid(this.cause);
}

class JSONDecodeError implements Exception {
    String cause;
    JSONDecodeError(this.cause);
}

class ImageMissing implements Exception {
    String cause;
    ImageMissing(this.cause);
}