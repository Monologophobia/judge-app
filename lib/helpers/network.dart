import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;

import '../models/user.dart';
import 'exceptions.dart';

class Network {

    // specify main url
    static final String apiPath = "https://leaderboards.superhumansports.com/api/";

    /// Do a POST with supplied email and password
    /// @param String url endpoint
    /// @param User Object with Map<String email, String password>
    /// @param Options payload Map<String key, dynamic> to merge with user object
    Future<Map> post(String url, User user, [Map payload]) async {

        Map _body    = {"email": user.email, "password": user.password};
        Uri endpoint = Uri.parse(apiPath + url);

        // merge payload
        if (payload != null) {
            payload.forEach((_key, _data) {
                _body[_key] = json.encode(_data);
            });
        }

        http.Response response = await http.post(endpoint, body: _body);

        _checkExceptions(response);

        Map _return;
        try {
            _return = json.decode(response.body);
            if (!(_return is Map)) throw JSONDecodeError("Response was not JSON");
        }
        catch (exception) {
            _return = {"response": response.body};
        }
        return _return;

    }

    Future<Map> get(String url) async {

        Uri endpoint = Uri.parse(apiPath + url);

        http.Response response = await http.get(endpoint);

        _checkExceptions(response);

        Map _return = {};
        try {
            _return['response'] = json.decode(response.body);
        }
        catch (exception) {
            _return['response'] = response.body;
        }

        return _return;

    }

    /// Get the http session cookie and store it
    /// Check the response for general exceptions and handle accordingly
    void _checkExceptions(response) {
        if (response.statusCode == 302 || response.statusCode == 301 || response.statusCode == 404) {
            throw NetworkError("Route Not Found");
        }
        if (response.statusCode == 401) {
            throw UserInvalid(response.body);
        }
        if (response.statusCode == 500) {
            throw CredentialsInvalid(response.body);
        }
        if (response.statusCode != 200) {
            throw NetworkError(response.body);
        }
    }

}