import 'dart:async';
import 'dart:io' as io;

// path tools
import 'package:path/path.dart';
// sqlite
import 'package:sqflite/sqflite.dart';
// application path tools
import 'package:path_provider/path_provider.dart';

class DB {

    // set up database reference
    static Database _db;
    Future<Database> get db async {
        // if db already defined, return it
        if(_db != null) return _db;
        // otherwise initialise it
        _db = await initDatabase();
        return _db;
    }

    /// initialise database
    initDatabase() async {
        // get platform specific private app dir
        io.Directory appDir = await getApplicationDocumentsDirectory();
        // use platform specific directory notation to create path
        String path = join(appDir.path, "app.db");
        // open the database, with on create and on upgrade if needed
        var db = await openDatabase(path, version: 3, onCreate: _onCreate, onUpgrade: _onUpgrade);
        return db;
    }

    /// create the tables for the database
    void _onCreate(Database db, int version) async {
        await db.execute("CREATE TABLE results("
            "id INTEGER PRIMARY KEY,"
            "team_id INTEGER,"
            "event_id INTEGER,"
            "results TEXT,"
            "notes TEXT,"
            "submitted TEXT);");
    }

    void _onUpgrade(Database db, int oldVersion, int newVersion) async {
        if (oldVersion == 1 && newVersion == 2) {
            var database = await db;
            await database.execute("ALTER TABLE results ADD COLUMN submitted TEXT;");
        }
        if (oldVersion == 1 && newVersion == 3) {
            var database = await db;
            await database.execute("ALTER TABLE results ADD COLUMN submitted TEXT;");
            await database.execute("ALTER TABLE results ADD COLUMN notes TEXT;");
        }
        if (oldVersion == 2 && newVersion == 3) {
            var database = await db;
            await database.execute("ALTER TABLE results ADD COLUMN notes TEXT;");
        }
    }

    /// A simple insert method
    ///
    /// @param String table name to insert to
    /// @param Map of data to insert
    Future<int> insert(String table, Map<String, dynamic> data) async {
        var database = await db;
        int query = await database.insert(table, data);
        return query;
    }

    /// Insert many items at once
    ///
    /// @param String table name
    /// @param List of Map data
    Future<void> insertMany(String table, List<Map> data) async {
        for (Map _item in data) {
            Map<String, dynamic> _data = new Map.from(_item);
            await this.insert(table, _data);
        }
    }

    /// A simple delete all method
    ///
    /// @param String table name to delete
    Future<int> deleteAll(String table) async {
        var database = await db;
        int query    = await database.delete(table);
        return query;
    }

    /// Delete records with arguments
    ///
    /// @param String table to query
    /// @param String where with placeholders "column1 = ? AND column2 = ?"
    /// @param List of arguments [26, 'test']
    /// @return Future boolean
    Future<int> deleteWhere(String table, String where, List arguments) async {
        var database = await db;
        var query    = await database.delete(table, where: where, whereArgs: arguments);
        return query;
    }

    /// Queries a database table and returns all results
    ///
    /// @param String table name
    Future<List<Map>> queryAll(String table) async {
        var database = await db;
        var query    = await database.query(table);
        return query;
    }

    /// Queries the database with arguments
    ///
    /// @param String table to query
    /// @param List of columns to retrieve [column1, column2, etc]
    /// @param String where with placeholders "column1 = ? AND column2 = ?"
    /// @param List of arguments [26, 'test']
    /// Would give a SELECT column1, column2, etc FROM table WHERE column1 = 26 AND column2 = 'test'
    Future<List<Map>> queryWhere(String table, List columns, String where, List arguments) async {
        var database = await db;
        var query    = await database.query(table, columns: columns, where: where, whereArgs: arguments);
        return query;
    }

    /// Updates a table with new data
    /// 
    /// @param String table to query
    /// @param String where with placeholders "column1 = ? AND column2 = ?"
    /// @param List of where arguments [26, 'test']
    /// @param Map of update data eg {'name': 'new name'}
    Future<int> update(String table, String where, List arguments, Map data) async {
        var database = await db;
        var query = database.update(table, data, where: where, whereArgs: arguments);
        return query;
    }

    /// Queries a database table for the first (latest) result
    ///
    /// @param String table to query
    /// @return Map
    Future<Map> first(String table) async {
        var database = await db;
        var query = await database.query(table, whereArgs: [table]);
        //var query    = await database.rawQuery("SELECT * FROM ? LIMIT 1", ["'user'"]);
        return query.isEmpty ? {} : query[0];
    }

}