import 'package:flutter/material.dart';

import '../pages/login.dart';
import '../pages/judge.dart';

final routes = {
    '/login': (BuildContext context) => new LoginPage(),
    '/judge': (BuildContext context) => new JudgeApp(),
};